package cito.examples.chat.jaxrs;

import javax.ws.rs.ApplicationPath;

/**
 * 
 * @author Daniel Siviter
 * @since v1.0 [27 Jul 2016]
 */
@ApplicationPath("/")
public class Application extends javax.ws.rs.core.Application { }
