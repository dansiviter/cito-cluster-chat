/*

 * Copyright 2016-2017 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cito.examples.chat.servlet;

import java.io.IOException;
import java.security.Principal;
import java.util.Objects;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;

import cito.servlet.HttpFilter;

/**
 * @author Daniel Siviter
 * @since v1.0 [22 Apr 2017]
 */
@WebFilter(value = { "/*"}, asyncSupported = true)
public class AuthFilter extends HttpFilter {
	@Override
	public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
	throws IOException, ServletException
	{
		chain.doFilter(new Request(request), response);
	}


	// --- Inner Classes ---

	private static class Request extends HttpServletRequestWrapper {
		private Principal principal;

		/**
		 * @param request
		 */
		public Request(HttpServletRequest request) {
			super(request);
		}

		@Override
		public Principal getUserPrincipal() {
			return this.principal;
		}

		@Override
		public String getRemoteUser() {
			return getUserPrincipal() != null ? getUserPrincipal().getName() : null;
		}

		@Override
		public void login(String username, String password) throws ServletException {
			this.principal = new BasicPrincipal(username);
		}

		@Override
		public void logout() throws ServletException {
			this.principal = null;
		}
	}

	private static class BasicPrincipal implements Principal {
		private final String username;

		public BasicPrincipal(final String username) {
			this.username = Objects.requireNonNull(username);
		}

		@Override
		public String getName() {
			return this.username;
		}

		@Override
		public int hashCode() {
			return Objects.hash(this.username);
		}

		@Override
		public boolean equals(final Object o) {
			if (this == o) {
				return true;
			}
			if (o instanceof BasicPrincipal) {
				final BasicPrincipal that = (BasicPrincipal) o;
				if (Objects.equals(this.username, that.username)) {
					return true;
				}
			}
			return false;
		}
	}
}
