package cito.examples.chat.service;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import cito.annotation.Body;
import cito.annotation.OnSend;
import cito.annotation.OnSubscribe;
import cito.annotation.OnUnsubscribe;
import cito.broker.Inspector;
import cito.event.Message;
import cito.examples.chat.api.ChatMessage;
import cito.examples.chat.api.User;
import cito.examples.chat.api.UsersChanged;
import cito.server.MessagingSupport;
import cito.server.SecurityContext;

/**
 * 
 * @author Daniel Siviter
 * @since v1.0 [29 Jul 2016]
 */
@ApplicationScoped
public class ChatService {
	@Inject
	private MessagingSupport support;
	@Inject
	private Inspector inspector;

	private final List<ChatMessage> messages = new LinkedList<>(); // FIXME not distributed
//	private final Set<User> users = new HashSet<>();

	@PostConstruct
	public void init() {
		final OffsetDateTime now = OffsetDateTime.now(ZoneOffset.UTC);
		messages.add(new ChatMessage(now, "AnotherUser", "Hey all!"));
		messages.add(new ChatMessage(now.minus(1, ChronoUnit.MINUTES), "AnotherUser1", "G'day!"));
	}

	/**
	 * 
	 * @param msg
	 * @param securityCtx
	 */
	public void onUserSubscribe(@Observes @OnSubscribe("topic/users") Message msg, SecurityContext securityCtx) {
//		final User user = new User(msg.sessionId(), "bob"/*securityCtx.getUserPrincipal().getName()*/, OffsetDateTime.now(ZoneOffset.UTC));
//		this.users.add(user);
//		this.support.broadcast("topic/users", new UsersChanged(user, true));
//		inspector.
	}

	/**
	 * 
	 * @param msg
	 * @param securityCtx
	 */
	public void onUserUnsubscribe(@Observes @OnUnsubscribe("topic/users") Message msg, SecurityContext securityCtx) {
//		final User user = new User(msg.sessionId(), "bob"/*securityCtx.getUserPrincipal().getName()*/, null);
//		this.users.remove(user);
//		this.support.broadcast("topic/users", new UsersChanged(user, false));
	}

	/**
	 * 
	 * @param msg
	 */
	public void onWelcomeChat(@Observes @OnSubscribe("topic/chat") Message msg, SecurityContext securityCtx) {
		final OffsetDateTime now = OffsetDateTime.now(ZoneOffset.UTC);
		final String user = "bob"/*securityCtx.getUserPrincipal().getName()*/;
		final ChatMessage chatMessage = new ChatMessage(now, user, "Welcome " + user + "!");
		this.support.sendTo(msg.sessionId(), msg.frame().destination(), chatMessage);
	}

	/**
	 * 
	 * @param msg
	 */
	public void onChat(@Observes @OnSend("app/chat") Message msg, SecurityContext securityCtx, @Body Map<String, String> payload) {
		final OffsetDateTime now = OffsetDateTime.now(ZoneOffset.UTC);
		final ChatMessage chatMessage = new ChatMessage(now, "bob"/*securityCtx.getUserPrincipal().getName()*/, payload.get("msg"));

		this.messages.add(chatMessage);
		final OffsetDateTime removeBefore = now.minus(10, ChronoUnit.MINUTES);
		this.messages.removeIf(m -> m.time.isBefore(removeBefore));
		this.support.broadcast("topic/chat", msg);
	}
}
