import styles from './index.scss';
import React from 'react';
import ReactDOM from 'react-dom';

import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';

import LoginDialog from './components/LoginDialog';
import PeopleList from './components/PeopleList';
import Messages from './components/Messages';
import Sender from './components/Sender';

import Stomp from './Stomp';

const muiTheme = getMuiTheme({ });

/*
 * @class App
 * @extends React.Component
 */
class App extends React.Component {
    constructor(props) {
        super(props);
        this.handleOpenLogin = this.handleOpenLogin.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }

    componentDidMount() {
        this.handleOpenLogin();
    }

    /*
     * 
     */
    handleOpenLogin() {
        this.loginDialog.handleOpen();
    }

    /*
     * 
     */
    handleLogin(login, success) {
        // password is fixed for convenience rather than security
        Stomp.connect(login, 'password', success);
    }

    /*
     * @method render
     * @returns {JSX}
     */
    render() {
        return <MuiThemeProvider muiTheme={muiTheme} style={{ height: '100%' }}>
            <div style={{ height: '100%' }}>
                <AppBar title='Cito Chat' showMenuIconButton={ false } />
                <div style={{ height: '100%', width: '100%', display: 'flex', overflow: 'hidden' }}>
                    <PeopleList style={{ height: '100%', overflow: 'hidden' }} />
                    <div style={{ height: '100%', width: '100%', display: 'flex', flexDirection: 'column' }}>
                        <Messages style={{ height: '100%' }}/>
                        <Sender />
                    </div>
                </div>
                <LoginDialog ref={(loginDialog) => this.loginDialog = loginDialog} onLogin={ this.handleLogin }/>
            </div>
        </MuiThemeProvider>;
    }
}

export default App;