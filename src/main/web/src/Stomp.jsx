import webstomp from 'webstomp-client';

export const DISCONNECTED = 'DISCONNECTED';
export const CONNECTING = 'CONNECTING';
export const RECONNECTING = 'RECONNECTING';
export const CONNECTED = 'CONNECTED';
export const DISCONNECTING = 'DISCONNECTING';

const uri = 'ws://' + window.location.hostname + ':' + window.location.port + '/websocket';

function createClient() {
    return webstomp.client(uri, { 
        protocols: [ 'v12.stomp' ],
        binary: false,
        heartbeat: { outgoing: 10000, incoming: 10000 },
        debug: true });
}

/*
 * @class Connection
 */
class Stomp {
    constructor() {
        this.subIdCount = 0;
        this.subscriptions = { };
        this.status = DISCONNECTED;
    }

    setStatus(status) {
        if (this.status == status) {
            return;
        }
        this.status = status;
        console.log('Citō connection state changed. [' + this.status + ']');
        if (this.status == CONNECTED) {
            this.resubscribe();
        }
    }

    connect(login, passcode, success) {
        if (this.stomp) {
            throw 'Stomp client already exists!';
        }

        this.login = login;
        this.passcode = passcode;
        this.success = success;

        this.stomp = createClient();

        this.setStatus(CONNECTING);
        var headers = { };
        if (login) {
            headers.login = login;
        }
        if (passcode) {
            headers.passcode = passcode;
        }

        this.stomp.connect(
                headers,
                (frame) => {
                    this.setStatus(CONNECTED);
                    if (success) {
                        success();
                    }
                },
                (error) => {
                    console.log('Error from Citō: ' + error);
                    if (error instanceof CloseEvent) {
                        if (error.wasClean) {
                            this.setStatus(DISCONNECTED);
                            this.stomp = null;
                        } else {
                            this.reconnect();
                        }
                    }
                }
        );
    }

    reconnect() {
        this.setStatus(RECONNECTING);
        this.stomp = null;
        const reconInv = setInterval(() => {
          if (this.status != RECONNECTING) {
              clearInterval(reconInv);
              return;
          }
          this.connect(this.login, this.passcode, this.success);
        }, 1000);
    }

    close() {
        this.setStatus(DISCONNECTING);
        this.stomp.disconnect(() => {
            this.setStatus(DISCONNECTED);
        }, headers={})
    }

    subscribe(destination, callback, headers = {}) {
        const id = this.subIdCount++;
        const subscription = {
                destination: destination,
                callback: callback,
                headers: headers,
                stompSubscription: null,
                unsubscribe: (headers = {}) => {
                    this.unsubscribe(id, headers);
                }
        };
        this.subscriptions[id] = subscription;
        
        if (this.status == CONNECTED) {
            this.doSubscribe(id);
        }
        return subscription;
    }

    resubscribe() {
        Object.keys(this.subscriptions).forEach((id) => {
            this.doSubscribe(id);
        });
    }

    doSubscribe(subId) {
        const subscription = this.subscriptions[subId];
        const sub = this.stomp.subscribe(subscription.destination, subscription.callback, subscription.headers);
        subscription.stompSubscription = sub;
    }

    unsubscribe(id, headers = {}) {
        const subscription = this.subscriptions[id];
        if (this.status == CONNECTED) {
            subscription.stompSubscription.unsubscribe(headers)
        }
        delete this.subscriptions[id];
    }
}

export default (new Stomp);