import React from 'react';

import MessageBubble from './MessageBubble';

/*
 * @class Messages
 * @extends React.Component
 */
class Messages extends React.Component {
    constructor(props) {
        super(props);
    }

    /*
     * @method render
     * @returns {JSX}
     */
    render() {
        return <div style={{ width: '100%', display: 'flex', flexDirection: 'column' }} >
            <MessageBubble />
            <MessageBubble />
        </div>;
    }
}

export default Messages;