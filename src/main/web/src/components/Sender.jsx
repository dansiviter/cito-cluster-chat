import React from 'react';

/*
 * @class Sender
 * @extends React.Component
 */
class Sender extends React.Component {
    constructor(props) {
        super(props);
    }

    /*
     * @method render
     * @returns {JSX}
     */
    render() {
        return <div style={{ width: '100%', display: 'flex', flexDirection: 'row' }}>
            Sender!
        </div>;
    }
}

export default Sender;