import React from 'react';

import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';


/*
 * @class LoginDialog
 * @extends React.Component
 */
class LoginDialog extends React.Component {
    constructor(props) {
        super(props);
        this.state = { open: false };

        this.handleLogin = this.handleLogin.bind(this);
    }

    /*
     * @method handleClose
     */
    handleOpen() {
        this.setState({ open: true });
    };

    /*
     * @method handleClose
     */
    handleLogin() {
        const login = this.refs.loginField.getValue();
        this.props.onLogin(login, () => this.setState({ open: false }));
    };

    /*
     * @method render
     * @returns {JSX}
     */
    render() {
        const actions = [
            <RaisedButton
                label="Login"
                primary={ true }
                onTouchTap={ this.handleLogin } />
        ];

        var quote = this.state.quote;
        return <Dialog
            title="Login"
            actions={ actions }
            modal={ true }
            open={ this.state.open }>
                <div>
                Enter your username. Try a SouthPark character name otherwise you may not get access!
                </div>
                <TextField autoFocus 
                        floatingLabelText="Username"
                        floatingLabelFixed={ true }
                        hintText="eric, kyle, sheila, chef..."
                        ref="loginField"
                        onKeyDown={ e => { if (e.key == 'Enter') { this.handleLogin(); } } } />
            </Dialog>;
    }
};

export default LoginDialog;